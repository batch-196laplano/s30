// db.fruits.insertMany([
//     {
//         name:"Apple",
//         supplier :"Red Farms Inc.",
//         stocks:20, 
//         price :40,
//         onsale: true
//     },
//     {
//         name:"Banana",
//         supplier :"Yellow Farms",
//         stocks: 15, 
//         price : 20,
//         onsale: false
//     },
//     {
//         name:"Kiwi",
//         supplier :"Green Farming and Canning",
//         stocks:25, 
//         price :50,
//         onsale: true
//     },
//     {
//         name:"Mango",
//         supplier :"Yellow Farms",
//         stocks:10, 
//         price :60,
//         onsale: true
//     },
//     {
//         name:"Dragon Fruit",
//         supplier :"Red Farms Inc.",
//         stocks:10, 
//         price :60,
//         onsale: true
//     },
// ])


//aggregate
// match and group

db.fruits.aggregate([
    {$match: {onsale:true}}, //finds the document that is onsale:true
    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}} //group with the same supplier, created totalStocks $sum of stocks per group
])
 
    
db.fruits.aggregate([
    //finds the document that is onsale:true
    {$match: {onsale:true}}, 
    //group with all the supplier, created totalStocks $sum of stocks per group
    //{$group: {_id:null,totalStocks:{$sum:"$stocks"}}} 
    //group with all the supplier and named t AllStocks, created totalStocks $sum of stocks per group    
    {$group: {_id:"AllStocks",totalStocks:{$sum:"$stocks"}}} 
])
    
    
    
db.fruits.aggregate([
    //finds the document that is onsale:true
    {$match: {supplier:"Red Farms Inc."}}, 
    //group with all the supplier, created totalStocks $sum of stocks per group
    //{$group: {_id:null,totalStocks:{$sum:"$stocks"}}} 
    //group with all the supplier and named t AllStocks, created totalStocks $sum of stocks per group    
    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}} 
])
    
    

db.fruits.update({name:"Banana"},{$set:{onsale:false}})

//mini activity
db.fruits.aggregate([
    {$match: {$and:[{supplier:"Yellow Farms"},{onsale:true}]}},   
    {$group: {_id:"onSale",totalStocks:{$sum:"$stocks"}}} 
])

//$avg AVEGRAGE    
db.fruits.aggregate([
    {$match: {onsale:true}},
    {$group: {_id:"$supplier",avgStocks:{$avg:"$stocks"}}}
    ])
    
db.fruits.aggregate([
    {$match: {onsale:true}},
    {$group: {_id:null,avgPrice:{$avg:"$price"}}}
    ])
    
//$max MAX highest value
db.fruits.aggregate([
    {$match: {onsale:true}},
    {$group: {_id:"highestStockOnSale",maxStock:{$max:"$stocks"}}}
    ])

db.fruits.aggregate([
    {$match: {onsale:true}},
    {$group: {_id:null,maxStock:{$max:"$price"}}}
    ])
    
//$min MIN lowest value
db.fruits.aggregate([
    {$match: {onsale:true}},
    {$group: {_id:"lowestStockOnSale",minStock:{$min:"$stocks"}}}
    ])
    
db.fruits.aggregate([
    {$match: {onsale:true}},
    {$group: {_id:null,minPrice:{$min:"$price"}}}
    ])
//must be 15
db.fruits.aggregate([
    {$match: {price:{$lt:50}}},
    {$group: {_id:"lowestStock",minStock:{$min:"$stocks"}}}
    ])
    
//$count COUNT
    db.fruits.aggregate([
    {$match: {onsale:true}},
    {$count: "itemOnSale"}
    ])
    
    db.fruits.aggregate([
    {$match: {price:{$lt:50}}},
    {$count: "itemPriceLessThan50"}
    ])
    
    db.fruits.aggregate([
    {$match: {stocks:{$lt:20}}},
    {$count: "forRestock"}
    ])
    
//$out
    
    db.fruits.aggregate([
    {$match: {onsale:true}},
    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
    {$out:"StocksPerSupplier"} // creates new collection , overwrites when exists
    ])
    
//ACTIVITY//
db.fruits.aggregate([
    {$match: {$and:[{supplier:"Yellow Farms"},{price:{$lt:50}}]}},
    {$count: "Supplied by Yellow Farms"}
    ])   
    
db.fruits.aggregate([
    {$match: {$and:[{},{price:{$lt:30}}]}},
    {$count: "itemsLessThan30Price"}
    ])   

db.fruits.aggregate([
    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"$supplier",avgPrice:{$avg:"$price"}}}
    ])       
    
db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"$supplier",maxPrice:{$max:"$price"}}}
    ])  
   
db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"$supplier",minPrice:{$min:"$price"}}}
    ])    