db.fruits.insertMany([
    {
        name:"Apple",
        supplier :"Red Farms Inc.",
        stocks:20, 
        price :40,
        onsale: true
    },
    {
        name:"Banana",
        supplier :"Yellow Farms",
        stocks: 15, 
        price : 20,
        onsale: true
    },
    {
        name:"Kiwi",
        supplier :"Green Farming and Canning",
        stocks:25, 
        price :50,
        onsale: true
    },
    {
        name:"Mango",
        supplier :"Yellow Farms",
        stocks:10, 
        price :60,
        onsale: true
    },
    {
        name:"Dragon Fruit",
        supplier :"Red Farms Inc.",
        stocks:10, 
        price :60,
        onsale: true
    },
])

    
//ACTIVITY SOLUTION//
db.fruits.aggregate([
    {$match: {$and:[{supplier:"Yellow Farms"},{price:{$lt:50}}]}},
    {$count: "Supplied by Yellow Farms"}
    ])   
    
db.fruits.aggregate([
    {$match: {$and:[{},{price:{$lt:30}}]}},
    {$count: "itemsLessThan30Price"}
    ])   

db.fruits.aggregate([
    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"$supplier",avgPrice:{$avg:"$price"}}}
    ])       
    
db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"$supplier",maxPrice:{$max:"$price"}}}
    ])  
   
db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"$supplier",minPrice:{$min:"$price"}}}
    ])    